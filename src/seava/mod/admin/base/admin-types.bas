import global.base.*

package seava.mod.admin.base {

  data-domain TDataType : String max-length: 32
    static-values [ "string" , "text" , "integer" , "decimal" , "boolean" , "date" ] ;

  data-domain TAttachmentType : String max-length: 8
    static-values [ "link" , "upload" ] ;

  data-domain TMenuTag : String max-length: 32
    static-values [ "top" , "left" ] ;

  data-domain TNumberSeparator : String min-length: 1 max-length: 1
    static-values [ "." , "," ] ;

  data-domain TCmpType : String min-length: 1 max-length: 1
    static-values [ "frame-dcgrid" , "frame-dcegrid" ] ;

}